#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>

#include <wiringPi.h>

int
main (int argc, char *argv[])
{
  int pin1, pin2;
  if (argc < 3)
    {
      fprintf (stderr, "usage: %s GPIO1 GPIO2\n", argv[0]);
      return 1;
    }

  /* gpio pin numbering */
  if (wiringPiSetupGpio () == -1)
    {
      fprintf (stderr, "%s: Unable to initialise GPIO mode.\n", argv [0]) ;
      return 1;
    }
  
  pin1 = atoi (argv[1]);
  pin2 = atoi (argv[2]);

  { int i = 0, parity = 0;
    for (; i < 1000; ++i) 
      {
	digitalWrite (pin1, 1-parity);
	digitalWrite (pin2, parity);

	parity = 1 - parity;
	delay (50);
      }
    digitalWrite (pin1, LOW);
    digitalWrite (pin2, LOW);
  }

  return 0;
}
